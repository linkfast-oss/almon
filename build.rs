fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("wiserpc/wiserpc.proto")?;
    tonic_build::compile_protos("wiserpc/wisertc.proto")?;
    Ok(())
}