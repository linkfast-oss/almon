Almon Server

almon-server is the official implementation of the WiSeRTC extended 
communication protocol.

The WiSeRTC is a binary streaming extension for the WiSeRPC protocol allowing
bi-directional binary media streaming. Almon will require a WiSeRPC endpoint
implementing the arrangement and upgrade methods, this server can be external
or enabled in Almon itself. More information about WiSeRTC

