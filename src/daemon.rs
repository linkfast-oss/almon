mod rtc;
mod rpc;

use std::collections::{HashMap, HashSet};
use std::convert::Infallible;
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use tokio::net::{TcpListener};
use tokio::spawn;
use futures::{sink::SinkExt, stream::StreamExt};
use hyper::{Body, Request, Response};
use hyper::service::{make_service_fn, service_fn};
use hyper_tungstenite::{tungstenite, tungstenite::protocol::{CloseFrame, frame::coding::CloseCode}, HyperWebsocket, is_upgrade_request, upgrade};
use tonic::IntoRequest;
use tungstenite::Message;

pub mod almon_proto {
    tonic::include_proto!("wisertc");
}

pub mod wiserpc_proto {
    tonic::include_proto!("wiserpc");
}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;
type PeerMap = Arc<Mutex<HashMap<u8, tungstenite::WebSocket<std::net::TcpStream>>>>;

#[tokio::main]
async fn main() -> Result<(),Error> {
    let listen_addr: SocketAddr = "[::1]:8000".parse()?;
    let listener = TcpListener::bind(&listen_addr).await?;
    let clients : PeerMap = Arc::new(Mutex::new(HashMap::new()));
    println!("almon server listening on: {}", listen_addr);

    let mut hyper_http = hyper::server::conn::Http::new();
    hyper_http.http1_only(true);
    hyper_http.http1_keep_alive(true);

    loop {
        let (stream, _) = listener.accept().await?;
        let make_svc = make_service_fn(
            move |_| {
                let clients = clients.clone();
                async {
                    Ok::<_, Infallible>(service_fn( move |req| {
                        handle_request(req, clients.clone())
                    }))
                }
            }
        );
        let conn = hyper_http
            .serve_connection(stream, make_svc)
            .with_upgrades();
        spawn(async move {
            if let Err(err) = conn.await {
                println!("hyper: error serving http connection {:?}", err);
            }
        });
    }
}

async fn serve_websocket(websocket: HyperWebsocket, request_path: String) -> Result<(), Error> {
    let mut websocket = websocket.await?;

    while let Some(msg) = websocket.next().await {
        match msg? {
            Message::Text(_) => {
                let close_code = CloseFrame {
                    code: CloseCode::from(1007),
                    reason: "Only binary messages are supported by WiSeRPC.".to_string().into(),
                };
                websocket.close(Option::from(close_code)).await?;
            },
            Message::Binary(msg) => {
                if request_path.starts_with("/call") {
                    println!("received binary RPC data: {:02X?}", msg);
                    websocket.send(Message::binary(b"Thank you, come again.".to_vec())).await?;
                }
                if request_path.starts_with("/rtc/") {
                    println!("received RTC data: {:02X?}", msg);
                    websocket.send(Message::binary(b"MEDIA_DATA".to_vec())).await?;
                }
            },
            Message::Ping(msg) => {
                // No need to send a reply: tungstenite takes care of this for you.
                println!("Received ping message: {:02X?}", msg);
            },
            Message::Pong(msg) => {
                println!("Received pong message: {:02X?}", msg);
            }
            Message::Close(msg) => {
                // No need to send a reply: tungstenite takes care of this for you.
                if let Some(msg) = &msg {
                    println!("Received close message with code {} and message: {}", msg.code, msg.reason);
                } else {
                    println!("Received close message");
                }
            },
            Message::Frame(msg) => {
                unreachable!();
            }
        }
    }
    Ok(())
}

async fn handle_request(
    mut request: Request<Body>,
    clients: Arc<Mutex<HashSet<tokio::sync::mpsc::UnboundedSender<Message>>>>
) -> Result<Response<Body>, Error> {
    let request_path = request.uri().path().to_string();

    if is_upgrade_request(&request) {
        let (response, websocket) = upgrade(&mut request, None)?;

        spawn(
            async move {
                if let Err(e) = serve_websocket(websocket, request_path).await {
                    eprintln!("hyper: error in websocket connection {}", e);
                }
            }
        );
        Ok(response)
    } else {
        Ok(Response::new(Body::from("almon meeting server")))
    }
}


