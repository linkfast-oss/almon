use std::env;
use clap::{Parser, Subcommand, Args};
use rodio::{OutputStream, source::Source};
use cpal::{SampleRate, SampleFormat};
use cpal::traits::{DeviceTrait, HostTrait};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct CliArgs {
    #[command(subcommand)]
    command: CliCommands
}

#[derive(Subcommand, Debug)]
enum CliCommands {
    /// Joins or creates an WiSeRTC session
    Join(JoinCommand),
    /// Records a media stream to a file
    Record(RecordCommand),
}

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
struct RecordCommand {
    #[command(subcommand)]
    record_type: RecordType
}

#[derive(Args, Debug)]
struct JoinCommand {
    #[arg(short = 'H', long, default_value = "127.0.0.1")]
    /// Server to join
    host: String,
    #[arg(short = 'P', long, default_value_t = 8000)]
    /// Server WiSeRPC listening port
    port: u32,
    #[arg(short, long)]
    /// Session name to join or create
    session_name: Option<String>
}

#[derive(Subcommand, Debug)]
enum RecordType {
    /// Records an audio file
    Audio(RecordAudioCommand),
}

#[derive(Args, Debug)]
struct RecordAudioCommand {
    #[arg(short, long)]
    /// Output file
    output: Option<String>,
    #[arg(short, long)]
    /// Codec
    codec: Option<String>
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Parse Arguments
    let args = CliArgs::parse();
    // Set up logging
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "info");
    }
    env_logger::init();
    // Arguments Behaviour
    match &args.command {
        CliCommands::Join(join_args) => {
            match join_args.session_name.as_ref() {
                Some(session_unwrap) => {
                    // list_folder(path_unwrap.to_string(), ls_args.json)
                    println!("Joining {}", session_unwrap)
                },
                None => {
                    log::error!("Please set a session name to join or create")
                }
            }
        },
        CliCommands::Record(record_type) => {
            match &record_type.record_type {
                RecordType::Audio(audio_args) => {
                    match audio_args.output.as_ref() {
                        Some(output_unwrap) => {
                            println!("Recording Audio!")
                        },
                        None => {
                            log::error!("Please set an output file name")
                        }
                    }
                }
            }
        }
        _ => {}
    }
    Ok(())
}